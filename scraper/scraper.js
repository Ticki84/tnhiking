const fs = require('fs')
const request = require('request');
const cheerio = require('cheerio');
const https = require('https')
const xml2js = require('xml2js')
const util = require('util');
const axios = require("axios").default;

const fethHtml = async url => {
    try {
        const { data } = await axios.get(url);
        return data;
    } catch {
        console.error(
            `ERROR: An error occurred while trying to fetch the URL: ${url}`
        );
    }
};

const getUrl = async (url, selector) => {
    const html = await fethHtml(url);
    const $ = cheerio.load(html);
    let urls = [];
    $(selector).each(function (i, element) {
        urls.push($(element).attr('href').trim());
    });
    return urls
};

const getData = async (url) => {
    const html = await fethHtml(url);
    const $ = cheerio.load(html);
    try {
        let name = ""
        let description = ""
        let activityType = ""
        let commune = {}
        let photos = {
            photo: []
        }
        let steps = {
            step: []
        }
        let practicalInformations = {
            practicalInformation: []
        }
        let sightWorthinesses = {
            sightWorthiness: []
        }
        let trace = ""

        let $ = cheerio.load(html);
        let elt = $('h1[itemprop="name"]')
        if (elt) {
            name = elt.text().trim()
            description = elt.next().text().trim()
        }
        elt = elt.next().next()
        while (elt && elt.get(0) && elt.get(0).tagName === "p") {
            practicalInformations.practicalInformation.push(elt.text().trim().replace(/<\/?[^>]+(>|$)/g, ""))
            elt = elt.next()
        }
        elt = $('img[title="Retour point de départ"]')
        if (elt) activityType = elt.next().next().next().next().text().trim()
        elt = $('img[title="Commune"]')
        if (elt) {
            commune.name = elt.next().next().text().trim()
            commune.zipCode = elt.next().next().get(0).next.data.trim().replace(/\D/g, "")
        }
        elt = $('img[title="Région"]')
        if (elt) commune.region = elt.next().next().text().trim()
        $('.homeListPhoto').each(function (i, element) {
            photos.photo.push($(element).children('img').eq(0).attr('src').replace('thumbnail\/t-', 'original\/'))
        })
        elt = $('h2')
        for (let i=0; i<elt.get().length; i++) {
            if (elt.eq(i).text().trim() === "Description de la randonnée") {
                elt = elt.eq(i).next()
                while (elt && elt.get(0) && elt.get(0).tagName === "p") {
                    steps.step.push(elt.text().trim().replace(/<\/?[^>]+(>|$)/g, ""))
                    elt = elt.next()
                }
                break
            }
        }
        elt = $('h2')
        for (let i=0; i<elt.get().length; i++) {
            if (elt.eq(i).text().trim() === "Informations pratiques") {
                elt = elt.eq(i).next()
                while (elt && elt.get(0) && elt.get(0).tagName === "p") {
                    practicalInformations.practicalInformation.push(elt.text().trim().replace(/<\/?[^>]+(>|$)/g, ""))
                    elt = elt.next()
                }
                break
            }
        }
        elt = $('h2')
        for (let i=0; i<elt.get().length; i++) {
            if (elt.eq(i).text().trim() === "Pendant la rando ou à proximité") {
                elt = elt.eq(i).next()
                while (elt && elt.get(0) && elt.get(0).tagName === "p") {
                    sightWorthinesses.sightWorthiness.push(elt.text().trim().replace(/<\/?[^>]+(>|$)/g, ""))
                    elt = elt.next()
                }
                break
            }
        }
        elt = $('#downloadGPXDatasheetAnonymous')
        if (elt) {
            trace = elt.attr('data')
            if (trace) {
                trace = trace.trim().substring(trace.lastIndexOf("=") + 1)
                trace = url + trace + ".gpx"

                return { name, description, activityType, commune, photos, steps, practicalInformations, sightWorthinesses, trace }
            }
        }
    } catch (e) {
        console.log(e)
    }
};

async function scrap() {
    /*let data = await getData("https://www.visorando.com/randonnee-decouverte-du-vieux-gray/")
    console.log(data)
    await writeGPX(data);*/

    let zipList = await getUrl('https://www.visorando.com/plan.html', 'body > main > div > div > div.col-md-9.col-lg-8.content-col > div > ul:nth-child(6) > li:nth-child(1) > ul > li > ul > li > a')
    console.log("Found "+zipList.length+" zip");

    let promises = [];
    let communeList = new Set();
    for (let i = 0; i < zipList.length; i ++) {
        //for (let i = 0; i < 1; i ++) {
        if (i > 0 && (i % 3 === 0 || (i === zipList.length - 1))) {
            await Promise.allSettled(promises).
            then((results) => results.forEach(result => {
                if (result.status === 'fulfilled') {
                    result.value.forEach(v => communeList.add(v))
                }}))
            promises = []
        }
        promises.push(getUrl(zipList[i], 'body > main > div > div > div.col-md-9.col-lg-8.content-col > div.search-block.margin-top > div > table > tbody > tr > td > a'))
    }
    await Promise.allSettled(promises).
    then((results) => results.forEach(result => {
        if (result.status === 'fulfilled') {
            result.value.forEach(v => communeList.add(v))
        }}))
    communeList = Array.from(communeList)
    console.log("Found "+communeList.length+" communes")

    promises = [];
    let hikeList = new Set();
    for (let i = 0; i < communeList.length; i ++) {
        //for (let i = 0; i < 5; i ++) {
        if (i > 0 && (i % 3 === 0 || (i === communeList.length - 1))) {
            await Promise.allSettled(promises).
            then((results) => results.forEach(result => {
                if (result.status === 'fulfilled') {
                    result.value.forEach(v => hikeList.add(v))
                }}))
            promises = []
        }
        promises.push(getUrl(communeList[i], 'body > main > div > div > div.col-md-9.col-lg-8.content-col > div.innerContentWithHeader > div.content-innerContentWithHeader > div > div.rando-title-sansDetail > a'))
    }
    await Promise.allSettled(promises).
    then((results) => results.forEach(result => {
        if (result.status === 'fulfilled') {
            result.value.forEach(v => hikeList.add(v))
        }}))
    hikeList = Array.from(hikeList)
    console.log("Found "+hikeList.length+" hikes")

    promises = [];
    for (let i = 0; i < hikeList.length; i ++) {
        //for (let i = 0; i < 5; i ++) {
        if (i > 0 && (i % 3 === 0 || (i === hikeList.length - 1))) {
            await Promise.allSettled(promises).
            then((results) => results.forEach(result => {
                if (result.status === 'fulfilled') {
                    writeGPX(result.value);
                }}))
            promises = []
        }
        promises.push(getData(hikeList[i]))
    }
    await Promise.allSettled(promises).
    then((results) => results.forEach(result => {
        if (result.status === 'fulfilled') {
            writeGPX(result.value);
        }}))
}

const writeGPX = async(value) => {
    try {
         await axios({
            url: value.trace,
            method: 'GET',
            responseType: 'blob', // important
        }).then((res) => {
            if (res.status >= 200 && res.status < 400) {
                xml2js.parseString(res.data, function(err, result) {
                    try {
                        if (!err) {
                            result.gpx.$.creator = "TNHiking"
                            result.gpx.metadata = { name: value.name, desc: value.description }
                            let name = '../gpx/'+value.name.replace(/[ &\/\\#,+()$~%.'":*?<>{}]/g, "_").normalize("NFD").replace(/[\u0300-\u036f]/g, "")+'.gpx'
                            delete value.name
                            delete value.description
                            delete value.trace
                            result.gpx.extensions = value
                            const builder = new xml2js.Builder();
                            const xml = builder.buildObject(result);
                            // Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15)
                            fs.writeFile(name, xml, (err) => {
                                if (err) {
                                    throw err;
                                }
                            });
                            //console.log(util.inspect(result, false, null))
                        } else console.log("cannot parse xml")
                    } catch (e) {
                        console.log("Wrong gpx format: "+e)
                    }
                });
            }
        }).catch(e => console.log(e));
    } catch (e) {
        console.log(e)
    }
}

scrap()