# Coding Week - groupe _Deprecated_

## Création d'une application en JavaFX

Florent Caspar <<florent.caspar@telecomnancy.eu>> 

Paul Chipot <<paul.chipot@telecomnancy.eu>>

Yann Colomb <<yann.colomb@telecomnancy.eu>>  

Benjamin Sepe <<benjamin.sepe@telecomnancy.eu>>  




> TelecomNancy Hiking est une application qui permet de préparer et de partager ses plus belles
> promenades et ses parcours de randonnées.

Vous trouverez la description détailée du projet dans le fichier [sujetCodingWeek.pdf](./sujetCodingWeek.pdf).


## Exécution

Vous pouvez exécuter le programme à l'aide de la commande :
```bash
$ ./gradlew run
```
## Compilation en un binaire distribuable

Vous pouvez compiler le code en un format .zip à l'aide de la commande :
```bash
$ ./gradlew build
```
Un zip distribuable sera alors généré dans `build/distributions`.

Chaque tag lance également l'exécution d'une pipeline GitLab qui compile le projet ; le distribuable résultant peut être téléchargé en artéfact de cette pipeline.

## Utilisation

Le dossier `sample_gpx` contient des exemples de fichiers GPX pouvant être importés dans l'application.

## Notes

-   TelecomNancy Hiking stocke ses données dans le fichier `$HOME/.tnhiking.db`.
    
-   Assurez-vous de quitter proprement l'application (pas d'interruption en ligne de commande), sinon la base de données ne sera pas mise à jour correctement.

## Planning

- Jour 0 : configuration de Gradle -> exécution et compilation du projet en un distribuable.
- Jour 1 : conception du programme, création de la vue principale ainsi que de la vue carte et les contrôleurs correspondants, création de la base du modèle.
- Jour 2 : création du modèle base de données SQLite, création de la liste de randonnées, création d'un scrapper pour le site Visorando, complétion des modèles, interaction modèles et carte, exportation d'une randonnée en GPX et tracé des randonnées sur la carte.
- Jour 3 : centrage carte sur randonnée après clic, importation de fichier GPX, création de la vue détaillée pour une randonnée, recherche d'une randonnée.
- Jour 4 : dessin d'une randonnée sur la carte, création du graphe d'altitude, ajout de filtres avancés et la vue correspondante, nettoyage de code.
- Jour 5 : Nettoyage de code, bugfix, documentation du projet, tournage et montage de la vidéo de démonstration.

## Vidéo de présentation

https://www.youtube.com/watch?v=D7weAFA2YGk