package TNHiking.models;

import TNHiking.enums.ActivityType;
import TNHiking.enums.Difficulty;
import com.sothawo.mapjfx.Coordinate;
import com.sothawo.mapjfx.Extent;
import io.jenetics.jpx.GPX;
import io.jenetics.jpx.WayPoint;
import javafx.application.Platform;
import org.json.JSONArray;

import java.nio.file.Paths;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class Database {
    private static Database instance;
    private final Connection dbConn;

    private Database() throws ClassNotFoundException, SQLException {
        Class.forName("org.sqlite.JDBC");
        String dbPath = Paths.get(System.getProperty("user.home"), ".tnhiking.db").toString();
        dbConn = DriverManager.getConnection("jdbc:sqlite:" + dbPath);

        Statement stmt = dbConn.createStatement();

        String createTable = getSQLResource("hikes.sql");
        stmt.executeUpdate(createTable);
        stmt.close();
    }

    public static Database getInstance() {
        if (instance == null) {
            try {
                instance = new Database();
            } catch (Exception e) {
                System.err.println("Error connecting to database.");
                Platform.exit();
            }
        }
        return instance;
    }

    public String GPXToString(GPX gpx) {
        return GPX.writer().toString(gpx);
    }

    public void saveHike(Hike hike) {
        try {
            String insertHikes = getSQLResource("hikes_insert.sql");
            PreparedStatement pstmt = dbConn.prepareStatement(insertHikes);

            if (hike.getId() != null)
                pstmt.setInt(1, hike.getId());
            pstmt.setString(2, hike.getName());
            pstmt.setString(3, hike.getDescription());

            if (hike.getCommune() != null) {
                pstmt.setString(4, hike.getCommune().getName());
                pstmt.setString(5, hike.getCommune().getZipCode());
                pstmt.setString(6, hike.getCommune().getRegion());
            }
            if (hike.getActivityType() != null) {
                pstmt.setString(7, hike.getActivityType().name());
            }
            if (hike.getDifficulty() != null) {
                pstmt.setString(8, hike.getDifficulty().name());
            }
            pstmt.setInt(9, hike.getDistance());
            pstmt.setInt(10, hike.getElevationGain());
            pstmt.setInt(11, hike.getElevationLoss());
            pstmt.setInt(12, hike.getHighestSpot());
            pstmt.setInt(13, hike.getLowestSpot());
            if (hike.getDuration() != null) {
                pstmt.setLong(14, hike.getDuration().toSeconds());
            }
            pstmt.setString(15, hike.getPracticalInformations());
            pstmt.setString(16, hike.getSightWorthinesses());
            pstmt.setBoolean(17, hike.getFavorite());

            WayPoint p = hike.getStartPoint();
            pstmt.setDouble(18, p.getLatitude().doubleValue());
            pstmt.setDouble(19, p.getLongitude().doubleValue());

            Extent e = hike.getExtent();
            pstmt.setDouble(20, e.getMin().getLatitude());
            pstmt.setDouble(21, e.getMin().getLongitude());
            pstmt.setDouble(22, e.getMax().getLatitude());
            pstmt.setDouble(23, e.getMax().getLongitude());
            pstmt.setString(24, ListToJSON(hike.getPhotos()));

            String xml = GPXToString(hike.getTrace());
            pstmt.setString(25, xml);

            pstmt.executeUpdate();

            if (hike.getId() == null) {
                hike.setId(pstmt.getGeneratedKeys().getInt(1));
            }
            pstmt.close();
        } catch (SQLException e) {
            System.err.println("Error saving hike in database.");
            e.printStackTrace();
        }
    }

    public void deleteHike(Hike hike) {
        if (hike.getId() == null) {
            System.err.println("DB error : attempt to delete hike with undefined id.");
            return;
        }
        try {
            String deleteHike = "DELETE FROM hikes WHERE id = ?;";
            PreparedStatement pstmt = dbConn.prepareStatement(deleteHike);
            pstmt.setInt(1, hike.getId());
            pstmt.executeUpdate();
            pstmt.close();
        } catch (SQLException e) {
            System.err.println("Error removing hike in database.");
            e.printStackTrace();
        }
    }

    public GPX getTrace(int id) {
        try {
            PreparedStatement pstmt = dbConn.prepareStatement("SELECT gpx_xml FROM hikes WHERE id = ?;");
            pstmt.setInt(1, id);
            ResultSet rs = pstmt.executeQuery();
            return GPX.reader().fromString(rs.getString(1));
        } catch (SQLException e) {
            System.err.println("Error getting trace for hike.");
            e.printStackTrace();
            return null;
        }
    }

    public ArrayList<Hike> getHikes() throws SQLException {
        String getHike = "SELECT * FROM hikes";
        Statement stmt = dbConn.createStatement();
        ResultSet rs = stmt.executeQuery(getHike);

        ArrayList<Hike> hikes = new ArrayList<>();

        while (rs.next()) {
            Commune commune = new Commune(rs.getString("commune"), rs.getString("commune_zip"), rs.getString("region"));
            String gpxStr = rs.getString("gpx_xml");
            GPX trace = GPX.reader().fromString(gpxStr);
            WayPoint startPoint = WayPoint.of(
                    rs.getDouble("start_point_lat"), rs.getDouble("start_point_long")
            );
            Coordinate min = new Coordinate(
                    rs.getDouble("extent_min_lat"), rs.getDouble("extent_min_long")
            );
            Coordinate max = new Coordinate(
                    rs.getDouble("extent_max_lat"), rs.getDouble("extent_max_long")
            );
            Extent extent = Extent.forCoordinates(min, max);
            Hike hike = new Hike(
                    rs.getInt("id"),
                    rs.getString("name"),
                    rs.getString("description"),
                    commune,
                    ActivityType.valueOf(rs.getString("activity_types")),
                    Difficulty.valueOf(rs.getString("difficulty")),
                    rs.getInt("distance"),
                    rs.getInt("elevation_gain"),
                    rs.getInt("elevation_loss"),
                    rs.getInt("highest_spot"),
                    rs.getInt("lowest_spot"),
                    rs.getInt("duration"),
                    rs.getString("practical_information"),
                    rs.getString("sight_worthiness"),
                    rs.getBoolean("is_favorite"),
                    startPoint,
                    extent,
                    JSONToList(rs.getString("images")),
                    trace
            );
            hikes.add(hike);
        }

        return hikes;
    }

    public ArrayList<Integer> searchHikes(String searchString) {
        ArrayList<Integer> results = new ArrayList<>();
        String searchHikes = getSQLResource("hikes_search.sql");
        try {
            PreparedStatement pstmt = dbConn.prepareStatement(searchHikes);
            pstmt.setString(1, searchString);
            pstmt.setString(2, searchString);
            pstmt.setString(3, searchString);
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                results.add(rs.getInt(1));
            }
        } catch (SQLException e) {
            System.err.println("Error searching hikes in database.");
            e.printStackTrace();
        }
        return results;
    }

    public ArrayList<Integer> searchHikes(ActivityType activity, Difficulty difficulty, Double minDuration, Double maxDuration, Double minDistance, Double maxDistance) {
        ArrayList<Integer> results = new ArrayList<>();
        try {
            final String filterString = "SELECT id FROM hikes WHERE %s %s duration >= ? AND duration <= ? AND distance >= ? AND distance <= ?;";

            String activityCheck = "";
            if (activity != ActivityType.ALL) {
                activityCheck = "activity_types = '" + activity.name() + "' AND";
            }

            String difficultyCheck = "";
            if (difficulty != Difficulty.ALL) {
                difficultyCheck = "difficulty = '" + difficulty.name() + "' AND";
            }

            PreparedStatement pstmt = dbConn.prepareStatement(String.format(filterString, activityCheck, difficultyCheck));
            pstmt.setInt(1, minDuration.intValue() * 3600);
            pstmt.setInt(2, maxDuration.intValue() * 3600);
            pstmt.setInt(3, minDistance.intValue() * 1000);
            pstmt.setInt(4, maxDistance.intValue() * 1000);
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                results.add(rs.getInt(1));
            }
        } catch (SQLException e) {
            System.err.println("Error filtering hikes in database.");
            e.printStackTrace();
        }
        return results;
    }

    public String getSQLResource(String name) {
        try {
            return new String(getClass().getResourceAsStream("/sql/" + name).readAllBytes());
        } catch (Exception e) {
            System.err.println("Error while fetching SQL file : " + name);
            System.err.println(e.toString());
            return null;
        }
    }

    public String ListToJSON(List<String> list) {
        JSONArray arr = new JSONArray(list);
        return arr.toString();
    }

    public ArrayList<String> JSONToList(String json) {
        List<Object> lst = new JSONArray(json).toList();
        ArrayList<String> outList = new ArrayList<>();
        lst.forEach(elt -> outList.add((String) elt));
        return outList;
    }

    public void close(Hikes hikes) {
        for (Hike hike : hikes.getHikes()) {
            saveHike(hike);
        }
        close();
    }

    public void close() {
        try {
            dbConn.close();
        } catch (SQLException e) {
            System.err.println("Error closing database.");
            e.printStackTrace();
        }
    }
}
