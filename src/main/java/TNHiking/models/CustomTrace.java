package TNHiking.models;

import com.sothawo.mapjfx.Coordinate;
import io.jenetics.jpx.GPX;
import io.jenetics.jpx.TrackSegment;
import io.jenetics.jpx.WayPoint;

import java.util.ArrayList;
import java.util.stream.Collectors;

public class CustomTrace {
    ArrayList<Coordinate> points = new ArrayList<>();

    public void addPoint(Coordinate point) {
        points.add(point);
    }

    public ArrayList<Coordinate> getPoints() {
        return points;
    }

    private WayPoint coordinateToWayPoint(Coordinate point) {
        return WayPoint.of(point.getLatitude(), point.getLongitude());
    }

    public GPX build() {
        return GPX.builder().addTrack(track -> track.addSegment(TrackSegment.of(
                points.stream().map(this::coordinateToWayPoint).collect(Collectors.toList())
        ))).build();
    }
}
