package TNHiking.models;

import TNHiking.listeners.CustomTracesListener;
import TNHiking.listeners.TraceDrawnListener;
import com.sothawo.mapjfx.Coordinate;
import io.jenetics.jpx.GPX;

import javax.swing.event.EventListenerList;
import java.util.ArrayList;

public class CustomTraces {
    private final EventListenerList listeners = new EventListenerList();
    private CustomTrace curTrace;

    public void startTrace() {
        curTrace = new CustomTrace();
        fireTraceUpdatedListener(true);
    }

    public void addPoint(Coordinate point) {
        curTrace.addPoint(point);
    }

    public ArrayList<Coordinate> getPoints() {
        return curTrace.getPoints();
    }

    public boolean isBuildingTrace() {
        return curTrace != null;
    }

    public void end() {
        CustomTrace trace = curTrace;
        curTrace = null;
        fireTraceUpdatedListener(false);
        if (trace.getPoints().size() == 0) return;
        GPX gpx = trace.build();
        fireTraceDrawnListeners(gpx);
    }

    public void addTracesListener(CustomTracesListener listener) {
        listeners.add(CustomTracesListener.class, listener);
    }

    public CustomTracesListener[] getCustomTracesListeners() {
        return listeners.getListeners(CustomTracesListener.class);
    }

    protected void fireTraceUpdatedListener(boolean isActive) {
        for (CustomTracesListener listener : getCustomTracesListeners()) {
            listener.traceUpdated(isActive);
        }
    }

    public void addTraceDrawnListener(TraceDrawnListener listener) {
        listeners.add(TraceDrawnListener.class, listener);
    }

    public TraceDrawnListener[] getTraceDrawnListeners() {
        return listeners.getListeners(TraceDrawnListener.class);
    }

    protected void fireTraceDrawnListeners(GPX trace) {
        for (TraceDrawnListener listener : getTraceDrawnListeners()) {
            listener.traceFinished(trace);
        }
    }
}
