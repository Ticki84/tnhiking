package TNHiking.models;

import com.sothawo.mapjfx.Coordinate;
import com.sothawo.mapjfx.Extent;
import io.jenetics.jpx.*;
import io.jenetics.jpx.geom.Geoid;

import java.util.stream.Collectors;

public class MapTools {
    public static Extent extentFromTrace(GPX trace) {
        return getLargerExtent(Extent.forCoordinates(trace.tracks()
                .flatMap(Track::segments)
                .flatMap(TrackSegment::points)
                .map(p -> new Coordinate(p.getLatitude().doubleValue(), p.getLongitude().doubleValue()))
                .collect(Collectors.toList())));
    }

    public static boolean extentsIntersect(Extent ext1, Extent ext2) {
        if (ext1.getMax().getLongitude() < ext2.getMin().getLongitude()) return false;
        if (ext2.getMax().getLongitude() < ext1.getMin().getLongitude()) return false;
        if (ext1.getMax().getLatitude() < ext2.getMin().getLatitude()) return false;
        return ext2.getMax().getLatitude() >= ext1.getMin().getLatitude();
    }

    public static Extent getLargerExtent(Extent extent) {
        Double latOffset = (extent.getMax().getLatitude() - extent.getMin().getLatitude()) * 0.25;
        Double longOffset = (extent.getMax().getLongitude() - extent.getMin().getLongitude()) * 0.25;
        Coordinate min = new Coordinate(
                extent.getMin().getLatitude() - latOffset,
                extent.getMin().getLongitude() - longOffset
        );
        Coordinate max = new Coordinate(
                extent.getMax().getLatitude() + latOffset,
                extent.getMax().getLongitude() + longOffset
        );
        return Extent.forCoordinates(min, max);
    }

    public static boolean shouldDraw(Extent inner, Extent outer) {
        if (!extentsIntersect(inner, outer)) return false;
        double latProp = (inner.getMax().getLatitude() - inner.getMin().getLatitude())
                / (outer.getMax().getLatitude() - outer.getMin().getLatitude());
        double longProp = (inner.getMax().getLongitude() - inner.getMin().getLongitude())
                / (outer.getMax().getLongitude() - outer.getMin().getLongitude());
        return latProp > 0.25 || longProp > 0.25;
    }

    public static int compareHikes(Coordinate c, Hike h1, Hike h2) {
        WayPoint center = WayPoint.of(c.getLatitude(), c.getLongitude());
        Length d1 = Geoid.WGS84.distance(center, h1.getStartPoint());
        Length d2 = Geoid.WGS84.distance(center, h2.getStartPoint());
        return d1.compareTo(d2);
    }
}
