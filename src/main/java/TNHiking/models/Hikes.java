package TNHiking.models;

import TNHiking.controllers.ControllerMap;
import TNHiking.enums.ActivityType;
import TNHiking.enums.Difficulty;
import TNHiking.listeners.HikesListener;
import TNHiking.listeners.MapPositionListener;
import TNHiking.tasks.DatabaseHikesTask;
import com.sothawo.mapjfx.Extent;

import javax.swing.event.EventListenerList;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Hikes {
    private final HashMap<Integer, Hike> hikes = new HashMap<>();
    private final EventListenerList listeners = new EventListenerList();
    private final Database db = Database.getInstance();

    public void initMapListener(ControllerMap mp) {
        mp.addInitListener(this::onMapInitialized);
    }

    private void onMapInitialized() {
        DatabaseHikesTask task = new DatabaseHikesTask();
        task.setOnSucceeded((event -> {
            ArrayList<Hike> taskHikes = task.getValue();
            try {
                for (Hike hike : taskHikes) {
                    addHike(hike);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }));

        ExecutorService executorService = Executors.newFixedThreadPool(1);
        executorService.execute(task);
        executorService.shutdown();
    }

    public void addHike(Hike hike) {
        db.saveHike(hike);
        hikes.put(hike.getId(), hike);
        fireHikesListeners();
    }

    public void removeHike(Hike hike) {
        hikes.remove(hike.getId());
        db.deleteHike(hike);
        fireHikesListeners();
    }

    public void filterHikes(String searchString) {
        ArrayList<Integer> results = Database.getInstance().searchHikes(searchString);

        hikes.forEach((id, hike) -> hike.setFiltered(true));
        results.forEach(id -> hikes.get(id).setFiltered(false));
        fireHikesListeners();
    }

    public void filterHikes() {
        hikes.forEach((id, hike) -> hike.setFiltered(false));
    }

    public void filterHikes(ActivityType activity, Difficulty difficulty, Double minDuration, Double maxDuration, Double minDistance, Double maxDistance) {
        ArrayList<Integer> results = Database.getInstance().searchHikes(activity, difficulty, minDuration, maxDuration, minDistance, maxDistance);

        hikes.forEach((id, hike) -> hike.setFiltered(true));
        results.forEach(id -> hikes.get(id).setFiltered(false));
        fireHikesListeners();
    }

    public ArrayList<Hike> getHikes() {
        return new ArrayList<>(hikes.values());
    }

    public void addHikesListener(HikesListener listener) {
        listeners.add(HikesListener.class, listener);
    }

    public HikesListener[] getHikesListeners() {
        return listeners.getListeners(HikesListener.class);
    }

    protected void fireHikesListeners() {
        for (HikesListener listener : getHikesListeners()) {
            listener.hikesUpdated();
        }
    }

    protected void fireHikeFavoriteListeners(Hike hike) {
        for (HikesListener listener : getHikesListeners()) {
            listener.newHikeFavorite(hike);
        }
    }

    public void addMapPositionListener(MapPositionListener listener) {
        listeners.add(MapPositionListener.class, listener);
    }

    public MapPositionListener[] getMapCenterListeners() {
        return listeners.getListeners(MapPositionListener.class);
    }

    protected void fireMapCenterListeners(Extent extent) {
        for (MapPositionListener listener : getMapCenterListeners()) {
            listener.mapCenterUpdated(extent);
        }
    }

    public void updateMapExtent(Extent extent) {
        fireMapCenterListeners(extent);
    }

    public void setFavorite(Hike hike, boolean isFavorite) {
        hike.setFavorite(isFavorite);
        if (isFavorite) fireHikeFavoriteListeners(hike);
    }
}
