package TNHiking.models;

import TNHiking.enums.ActivityType;
import TNHiking.enums.Difficulty;
import TNHiking.listeners.HikeListener;
import com.sothawo.mapjfx.Extent;
import io.jenetics.jpx.*;
import io.jenetics.jpx.geom.Geoid;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.swing.event.EventListenerList;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.IOException;
import java.io.StringReader;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

public class Hike {

    private final EventListenerList listeners = new EventListenerList();
    // Mandatory fields
    private Integer id = null;
    private String name = "";
    private String description = "";
    private Commune commune = new Commune("", "", "");
    private ActivityType activityType = ActivityType.FOOT;
    private Difficulty difficulty = Difficulty.EASY;
    private GPX trace = null;
    private boolean durationFromTrace = false;
    // Optional fields
    private ArrayList<String> photos = new ArrayList<>();
    private String steps = "";
    private String practicalInformations = "";
    private String sightWorthinesses = "";
    // Calculated fields
    private boolean backToStart = false;
    private int distance = 0; // in meter
    private int elevationGain = 0; // in meter
    private int elevationLoss = 0; // in meter
    private int highestSpot = 0; // in meter
    private int lowestSpot = 0; // in meter
    private Duration duration = Duration.ofSeconds(0);
    private boolean isFiltered = false;
    private boolean isFavorite = false;
    private Extent extent;
    private WayPoint startPoint;


    public Hike() {
    }

    public Hike(String name, String description, Commune commune, ActivityType activityType, GPX trace, boolean durationFromTrace) {
        this.name = name;
        this.description = description;
        this.commune = commune;
        this.activityType = activityType;
        this.durationFromTrace = durationFromTrace;
        this.trace = trace;
        updateCalculated();
        estimateDifficulty();
    }

    public Hike(Integer id, String name, String description, Commune commune, ActivityType activityType, Difficulty difficulty, int distance, int elevationGain, int elevationLoss, int highestSpot, int lowestSpot, int duration, String practicalInformation, String sightWorthiness, boolean isFavorite, WayPoint startPoint, Extent extent, ArrayList<String> photos, GPX trace) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.commune = commune;
        this.activityType = activityType;
        this.difficulty = difficulty;
        this.distance = distance;
        this.elevationGain = elevationGain;
        this.elevationLoss = elevationLoss;
        this.highestSpot = highestSpot;
        this.lowestSpot = lowestSpot;
        this.duration = Duration.ofSeconds(duration);
        this.practicalInformations = practicalInformation;
        this.sightWorthinesses = sightWorthiness;
        this.isFavorite = isFavorite;
        this.startPoint = startPoint;
        this.extent = extent;
        this.photos = photos;
        this.trace = trace;
    }

    public Hike(String name, String description, Commune commune, ActivityType activityType, GPX trace, boolean durationFromTrace, Difficulty difficulty) {
        this(name, description, commune, activityType, trace, durationFromTrace);
        this.difficulty = difficulty;
    }

    public static int distanceBeetween(TrackSegment segment, WayPoint start, WayPoint end) {
        int d = 0;
        List<WayPoint> wp = segment.getPoints().subList(segment.getPoints().indexOf(start), segment.getPoints().lastIndexOf(end) + 1);
        d += wp.stream().collect(Geoid.WGS84.toPathLength()).intValue();
        return d;
    }

    public void importFromGpx(String gpxPath) {
        if (gpxPath != null) {
            try {
                DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
                DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
                Document doc = dBuilder.parse(gpxPath);
                doc.getDocumentElement().normalize();
                XPath xPath = XPathFactory.newInstance().newXPath();

                ArrayList<Element> elName = evaluate(xPath, doc, "/gpx/metadata/name");
                if (elName.size() > 0) name = elName.get(0).getTextContent();
                ArrayList<Element> elDescription = evaluate(xPath, doc, "/gpx/metadata/desc");
                if (elDescription.size() > 0) description = elDescription.get(0).getTextContent();
                ArrayList<Element> elActivityType = evaluate(xPath, doc, "/gpx/extensions/activityType");
                if (elActivityType.size() > 0) {
                    switch (elActivityType.get(0).getTextContent()) {
                        case "A VTT":
                            activityType = ActivityType.BIKE;
                            break;
                        case "A cheval":
                            activityType = ActivityType.HORSE;
                            break;
                        case "A ski":
                            activityType = ActivityType.SKI;
                            break;
                        default:
                            activityType = ActivityType.FOOT;
                    }
                }
                ArrayList<Element> elCommuneName = evaluate(xPath, doc, "/gpx/extensions/commune/name");
                ArrayList<Element> elCommuneZipCode = evaluate(xPath, doc, "/gpx/extensions/commune/zipCode");
                ArrayList<Element> elCommuneRegion = evaluate(xPath, doc, "/gpx/extensions/commune/region");
                commune = new Commune(elCommuneName.size() > 0 ? elCommuneName.get(0).getTextContent() : null, elCommuneZipCode.size() > 0 ? elCommuneZipCode.get(0).getTextContent() : null, elCommuneRegion.size() > 0 ? elCommuneRegion.get(0).getTextContent() : null);
                ArrayList<Element> elPhotos = evaluate(xPath, doc, "/gpx/extensions/photos/photo");
                elPhotos.forEach(p -> photos.add(p.getTextContent()));
                ArrayList<Element> elSteps = evaluate(xPath, doc, "/gpx/extensions/steps/step");
                if (elSteps.size() > 0) steps = "";
                elSteps.forEach(p -> {
                    if (!steps.equals("")) steps += "\n";
                    steps += p.getTextContent();
                });
                ArrayList<Element> elPracticalInformations = evaluate(xPath, doc, "/gpx/extensions/practicalInformations/practicalInformation");
                if (elPracticalInformations.size() > 0) practicalInformations = "";
                elPracticalInformations.forEach(p -> {
                    if (!practicalInformations.equals("")) practicalInformations += "\n";
                    practicalInformations += p.getTextContent();
                });
                ArrayList<Element> elSightWorthinesses = evaluate(xPath, doc, "/gpx/extensions/sightWorthinesses/sightWorthiness");
                if (elSightWorthinesses.size() > 0) sightWorthinesses = "";
                elSightWorthinesses.forEach(p -> {
                    if (!sightWorthinesses.equals("")) sightWorthinesses += "\n";
                    sightWorthinesses += p.getTextContent();
                });
                trace = GPX.read(gpxPath);
                updateCalculated();
                estimateDifficulty();
                fireHikeListeners();
            } catch (ParserConfigurationException | XPathExpressionException | IOException | SAXException | IndexOutOfBoundsException e) {
                e.printStackTrace();
            }
        }
    }

    public void importDrawnTrace(GPX trace) {
        this.trace = trace;
        updateCalculated();
        estimateDifficulty();
        fireHikeListeners();
    }

    public void exportToGpx(String gpxPath) {
        if (trace != null) {
            try {
                // Reading current GPX
                DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
                DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
                InputSource is = new InputSource(new StringReader(GPX.writer().toString(trace)));
                Document doc = dBuilder.parse(is);
                doc.getDocumentElement().normalize();

                // Formatting extensions node
                Element eMetadata = doc.createElement("metadata");
                Element eExtensions = doc.createElement("extensions");

                Element eName = doc.createElement("name");
                eName.appendChild(doc.createTextNode("" + name));
                eMetadata.appendChild(eName);

                Element eDescription = doc.createElement("desc");
                eDescription.appendChild(doc.createTextNode("" + description));
                eMetadata.appendChild(eDescription);

                String fActivityType;
                switch (activityType) {
                    case HORSE:
                        fActivityType = "A cheval";
                        break;
                    case BIKE:
                        fActivityType = "A VTT";
                        break;
                    case SKI:
                        fActivityType = "A ski";
                        break;
                    default:
                        fActivityType = "A pied";
                }
                Element eActivityType = doc.createElement("activityType");
                eActivityType.appendChild(doc.createTextNode(fActivityType));
                eExtensions.appendChild(eActivityType);

                Element eCommune = doc.createElement("commune");
                Element eCommuneName = doc.createElement("name");
                eCommuneName.appendChild(doc.createTextNode(commune.getName()));
                eCommune.appendChild(eCommuneName);
                Element eCommuneZipCode = doc.createElement("zipCode");
                eCommuneZipCode.appendChild(doc.createTextNode(commune.getZipCode()));
                eCommune.appendChild(eCommuneZipCode);
                Element eCommuneRegion = doc.createElement("region");
                eCommuneRegion.appendChild(doc.createTextNode(commune.getRegion()));
                eCommune.appendChild(eCommuneRegion);
                eExtensions.appendChild(eCommune);

                Element ePhotos = doc.createElement("photos");
                for (String photo : photos) {
                    Element ePhoto = doc.createElement("photo");
                    ePhoto.appendChild(doc.createTextNode(photo));
                    ePhotos.appendChild(ePhoto);
                }
                eExtensions.appendChild(ePhotos);

                Element eSteps = doc.createElement("steps");
                for (String step : steps.split("\\r?\\n")) {
                    Element eStep = doc.createElement("step");
                    eStep.appendChild(doc.createTextNode(step));
                    eSteps.appendChild(eStep);
                }
                eExtensions.appendChild(eSteps);

                Element ePracticalInformations = doc.createElement("practicalInformations");
                for (String practicalInformation : practicalInformations.split("\\r?\\n")) {
                    Element ePracticalInformation = doc.createElement("practicalInformation");
                    ePracticalInformation.appendChild(doc.createTextNode(practicalInformation));
                    ePracticalInformations.appendChild(ePracticalInformation);
                }
                eExtensions.appendChild(ePracticalInformations);

                Element eSightWorthinesses = doc.createElement("sightWorthinesses");
                for (String sightWorthiness : sightWorthinesses.split("\\r?\\n")) {
                    Element eSightWorthiness = doc.createElement("sightWorthiness");
                    eSightWorthiness.appendChild(doc.createTextNode(sightWorthiness));
                    eSightWorthinesses.appendChild(eSightWorthiness);
                }
                eExtensions.appendChild(eSightWorthinesses);

                // Replacing or creating node
                XPath xPath = XPathFactory.newInstance().newXPath();
                ArrayList<Element> elGpx = evaluate(xPath, doc, "/gpx");
                ArrayList<Element> elMetadata = evaluate(xPath, doc, "/gpx/metadata");
                if (elMetadata.size() > 0) elMetadata.get(0).getParentNode().replaceChild(eMetadata, elMetadata.get(0));
                else if (elGpx.size() > 0) elGpx.get(0).appendChild(eMetadata);
                else throw new IOException();
                ArrayList<Element> elExtensions = evaluate(xPath, doc, "/gpx/extensions");
                if (elExtensions.size() > 0)
                    elExtensions.get(0).getParentNode().replaceChild(eExtensions, elExtensions.get(0));
                else if (elGpx.size() > 0) elGpx.get(0).appendChild(eExtensions);
                else throw new IOException();

                // Saving GPX
                TransformerFactory transformerFactory = TransformerFactory.newInstance();
                Transformer transformer = transformerFactory.newTransformer();
                transformer.setOutputProperty(OutputKeys.INDENT, "yes");
                transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
                DOMSource source = new DOMSource(doc);
                StreamResult result = new StreamResult(gpxPath);
                transformer.transform(source, result);
            } catch (IOException | ParserConfigurationException | SAXException | TransformerException | XPathExpressionException e) {
                e.printStackTrace();
            }
        }
    }

    // Getters/setters
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Commune getCommune() {
        return commune;
    }

    public void setCommune(Commune commune) {
        this.commune = commune;
    }

    public boolean getFiltered() {
        return isFiltered;
    }

    public void setFiltered(boolean isFiltered) {
        this.isFiltered = isFiltered;
    }

    public boolean getFavorite() {
        return isFavorite;
    }

    public void setFavorite(boolean isFavorite) {
        this.isFavorite = isFavorite;
    }

    public String getPracticalInformations() {
        return practicalInformations;
    }

    public void setPracticalInformations(String practicalInformations) {
        this.practicalInformations = practicalInformations;
    }

    public String getSightWorthinesses() {
        return sightWorthinesses;
    }

    public void setSightWorthinesses(String sightWorthinesses) {
        this.sightWorthinesses = sightWorthinesses;
    }

    public int getDistance() {
        return distance;
    }

    public int getElevationGain() {
        return elevationGain;
    }

    public int getElevationLoss() {
        return elevationLoss;
    }

    public int getHighestSpot() {
        return highestSpot;
    }

    public int getLowestSpot() {
        return lowestSpot;
    }

    public ActivityType getActivityType() {
        return activityType;
    }

    public void setActivityType(ActivityType activityType) {
        this.activityType = activityType;
        updateDuration();
    }

    public Difficulty getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(Difficulty difficulty) {
        this.difficulty = difficulty;
    }

    public boolean isBackToStart() {
        return backToStart;
    }

    public Duration getDuration() {
        return duration;
    }

    public GPX getTrace() {
        if (trace == null) {
            trace = Database.getInstance().getTrace(id);
        }
        return trace;
    }

    public ArrayList<String> getPhotos() {
        return photos;
    }

    public String getSteps() {
        return steps;
    }

    public void setSteps(String steps) {
        this.steps = steps;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public WayPoint getStartPoint() {
        return startPoint;
    }

    public boolean isDurationFromTrace() {
        return durationFromTrace;
    }

    public void setDurationFromTrace(boolean durationFromTrace) {
        this.durationFromTrace = durationFromTrace;
        updateDuration();
    }

    public Extent getExtent() {
        return extent;
    }

    public void setExtent(Extent extent) {
        this.extent = extent;
    }

    public void updateCalculated() {
        updateBackToStart();
        updateDistance();
        updateElevation();
        updateDuration();
        updateSpot();
        updatePoints();
    }

    public void updateBackToStart() {
        WayPoint[] wp = trace.tracks().flatMap(Track::segments).flatMap(TrackSegment::points).toArray(WayPoint[]::new);
        backToStart = Geoid.WGS84.distance(trace.getTracks().get(0).getSegments().get(0).getPoints().get(0), wp[wp.length - 1]).intValue() < 30;
    }

    public void updateDistance() {
        distance = 0;
        trace.tracks()
                .flatMap(Track::segments)
                .forEach(x -> distance += x.points()
                        .collect(Geoid.WGS84.toPathLength())
                        .intValue());
    }

    public void updatePoints() {
        this.extent = MapTools.extentFromTrace(trace);

        startPoint = trace.getTracks().get(0).getSegments().get(0).getPoints().get(0);
    }

    public void updateElevation() {
        elevationGain = 0;
        elevationLoss = 0;
        trace.tracks()
                .flatMap(Track::segments)
                .forEach(x -> {
                    List<WayPoint> wp = x.getPoints();
                    for (int i = 0; i < wp.size() - 1; i++) {
                        int alt = x.getPoints().get(i + 1).getElevation().orElse(Length.of(0, Length.Unit.METER)).intValue() - x.getPoints().get(i).getElevation().orElse(Length.of(0, Length.Unit.METER)).intValue();
                        if (alt >= 0) {
                            elevationGain += alt;
                        } else {
                            elevationLoss -= alt;
                        }
                    }
                });
    }

    public void updateSpot() {
        highestSpot = trace.tracks()
                .flatMap(Track::segments)
                .flatMap(TrackSegment::points)
                .map(x -> x.getElevation()
                        .orElse(Length.of(0, Length.Unit.METER))
                        .intValue())
                .max(Integer::compare).orElse(0);
        lowestSpot = trace.tracks()
                .flatMap(Track::segments)
                .flatMap(TrackSegment::points)
                .map(x -> x.getElevation()
                        .orElse(Length.of(0, Length.Unit.METER))
                        .intValue())
                .min(Integer::compare).orElse(0);
    }

    public void updateDuration() {
        if (trace != null) {
            if (durationFromTrace) { // we compute duration from gpx
                duration = Duration.ofSeconds(trace.tracks()
                        .flatMap(Track::segments)
                        .mapToInt(s -> (int) Duration.between(s.getPoints().get(0).getTime().orElseThrow(), s.getPoints().get(s.getPoints().size() - 1).getTime().orElseThrow()).toSeconds())
                        .sum());
            } else { // we compute duration from estimation
                // Warning: need computed distance and elevation gain
                // With a mean speed of 1 m/s on a flat surface
                switch (activityType) {
                    case BIKE:
                        duration = Duration.ofSeconds(((distance - elevationGain) + elevationGain * 10L) / 6L);
                        break;
                    case SKI:
                        duration = Duration.ofSeconds(distance / 5L + elevationGain / 2L);
                        break;
                    case HORSE:
                        duration = Duration.ofSeconds(distance / 4L);
                        break;
                    default:
                        duration = Duration.ofSeconds(((distance - elevationGain) + elevationGain * 10L));
                }
            }
        } else duration = Duration.ofSeconds(0);
        fireDurationListeners();
    }

    public void estimateDifficulty() {
        if (duration.toHours() <= 3 && distance <= 10000 && elevationGain <= 400) difficulty = Difficulty.EASY;
        else if (distance <= 15000 && elevationGain <= 700) difficulty = Difficulty.MEDIUM;
        else if (distance <= 20000) difficulty = Difficulty.HARD;
        else difficulty = Difficulty.REALLY_HARD;
    }

    public ArrayList<Element> evaluate(XPath xPath, Document doc, String expr) throws XPathExpressionException {
        ArrayList<Element> res = new ArrayList<>();
        NodeList nodeList = (NodeList) xPath.compile(expr).evaluate(doc, XPathConstants.NODESET);

        for (int i = 0; i < nodeList.getLength(); i++) {
            Node node = nodeList.item(i);
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                res.add((Element) node);
            }
        }
        return res;
    }

    public void addHikeListener(HikeListener listener) {
        listeners.add(HikeListener.class, listener);
    }

    public HikeListener[] getHikeListeners() {
        return listeners.getListeners(HikeListener.class);
    }

    protected void fireHikeListeners() {
        for (HikeListener listener : getHikeListeners()) {
            listener.hikeUpdated();
        }
    }

    protected void fireDurationListeners() {
        for (HikeListener listener : getHikeListeners()) {
            listener.durationUpdated();
        }
    }
}
