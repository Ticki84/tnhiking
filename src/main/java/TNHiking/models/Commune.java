package TNHiking.models;

public class Commune {

    private String name;
    private String zipCode;
    private String region;

    public Commune(String name, String zipCode) {
        this.name = name;
        this.zipCode = zipCode;
        this.region = null;
    }

    public Commune(String name, String zipCode, String region) {
        this.name = name;
        this.zipCode = zipCode;
        this.region = region;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }
}
