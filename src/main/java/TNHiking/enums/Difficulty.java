package TNHiking.enums;

public enum Difficulty {
    ALL("All"), EASY("Easy"), MEDIUM("Medium"), HARD("Hard"), REALLY_HARD("Really Hard");

    private final String label;

    Difficulty(String label) {
        this.label = label;
    }

    public String toString() {
        return label;
    }

}
