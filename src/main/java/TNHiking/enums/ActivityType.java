package TNHiking.enums;

public enum ActivityType {
    ALL("All"), FOOT("Foot"), BIKE("Bike"), HORSE("Horse"), SKI("Ski");

    private final String label;

    ActivityType(String label) {
        this.label = label;
    }

    public String toString() {
        return label;
    }

}
