package TNHiking.enums;

public enum MarkerType {
    HIKE, WAYPOINT, PLACE
}
