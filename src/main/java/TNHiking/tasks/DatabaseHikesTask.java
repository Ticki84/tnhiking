package TNHiking.tasks;

import TNHiking.models.Database;
import TNHiking.models.Hike;
import javafx.concurrent.Task;

import java.util.ArrayList;

public class DatabaseHikesTask extends Task<ArrayList<Hike>> {
    @Override
    protected ArrayList<Hike> call() throws Exception {
        Database db = Database.getInstance();
        return db.getHikes();
    }
}
