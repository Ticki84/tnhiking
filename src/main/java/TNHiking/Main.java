package TNHiking;

import TNHiking.controllers.ControllerMain;
import TNHiking.controllers.ControllerMap;
import TNHiking.controllers.ControllerPopup;
import TNHiking.models.CustomTraces;
import TNHiking.models.Database;
import TNHiking.models.Hikes;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;


public class Main extends Application {
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/fxml/mainView.fxml"));


        Hikes hikes = new Hikes();
        CustomTraces ct = new CustomTraces();

        ControllerMain cm = new ControllerMain(primaryStage, hikes, ct);
        ControllerPopup cp = new ControllerPopup(primaryStage, hikes, ct);
        ControllerMap mp = new ControllerMap(hikes, ct);

        hikes.initMapListener(mp);

        loader.setControllerFactory(ic -> {
            if (ic.equals(ControllerMain.class)) return cm;
            else if (ic.equals(ControllerPopup.class)) return cp;
            else if (ic.equals(ControllerMap.class)) return mp;
            else return null;
        });

        Parent root = loader.load();
        Scene scene = new Scene(root, 1400, 900);

        mp.initMapAndControls();
        primaryStage.getIcons().add(new Image(getClass().getResourceAsStream("/images/tnhiking.png")));
        primaryStage.setScene(scene);
        primaryStage.setTitle("TNHiking");
        primaryStage.setOnCloseRequest((WindowEvent event) -> Database.getInstance().close(hikes));

        primaryStage.show();
        cp.showPreviews();
    }
}
