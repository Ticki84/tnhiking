package TNHiking.controllers;

import TNHiking.enums.ActivityType;
import TNHiking.enums.Difficulty;
import TNHiking.listeners.HikeListener;
import TNHiking.models.Hike;
import TNHiking.models.Hikes;
import io.jenetics.jpx.GPX;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;

public class ControllerNewHike {


    final DirectoryChooser directoryChooser = new DirectoryChooser();
    private final Stage primaryStage;
    private final Hikes hikes;
    private final Stage stageNewHike;
    public BorderPane bpNewHike;
    public ComboBox<ActivityType> choiceActivityType;
    public Button butSelectFile;
    public Button butCancel;
    public Button butCreate;
    public TextField hikePostcode;
    public TextField hikeCommune;
    public TextArea hikeDescription;
    public TextField hikeName;
    public ComboBox<Difficulty> choiceDifficulty;
    public TextArea hikeProTips;
    public TextArea hikeSteps;
    public TextArea hikePlaceToSee;
    public TextField hikeRegion;
    public CheckBox hikeComputeTime;
    public TextField textGPX;
    public TextField textCompute;
    public Label hikeDuration;
    private Hike newHike;
    private GPX drawnTrace;

    public ControllerNewHike(Stage primaryStage, Hikes hikes, Stage stageNewHike) {
        this.primaryStage = primaryStage;
        this.hikes = hikes;
        this.stageNewHike = stageNewHike;
    }

    public ControllerNewHike(Stage primaryStage, Hikes hikes, Stage stageNewHike, GPX drawnTrace) {
        this.primaryStage = primaryStage;
        this.hikes = hikes;
        this.stageNewHike = stageNewHike;
        this.drawnTrace = drawnTrace;
    }

    public void initialize() {
        // Select Activity Type :
        ObservableList<ActivityType> at = FXCollections.observableArrayList(ActivityType.values());
        at.remove(ActivityType.ALL);
        choiceActivityType.setItems(at);
        choiceActivityType.getSelectionModel().selectFirst();

        // Select Difficulty :
        ObservableList<Difficulty> d = FXCollections.observableArrayList(Difficulty.values());
        d.remove(Difficulty.ALL);
        choiceDifficulty.setItems(d);
        choiceDifficulty.getSelectionModel().selectFirst();

        if (drawnTrace != null) {
            butSelectFile.setVisible(false);
            hikeComputeTime.setVisible(false);
            this.textGPX.setVisible(false);
            this.textCompute.setVisible(false);
            this.hikeDuration.setVisible(false);
        }

        // Create hike :
        resetHike();

        // Listeners:
        hikeName.textProperty().addListener((observable, oldValue, newValue) -> newHike.setName(newValue));
        hikeDescription.textProperty().addListener((observable, oldValue, newValue) -> newHike.setDescription(newValue));
        hikeCommune.textProperty().addListener((observable, oldValue, newValue) -> newHike.getCommune().setName(newValue));
        hikePostcode.textProperty().addListener((observable, oldValue, newValue) -> newHike.getCommune().setZipCode(newValue));
        hikeRegion.textProperty().addListener((observable, oldValue, newValue) -> newHike.getCommune().setRegion(newValue));
        hikePlaceToSee.textProperty().addListener((observable, oldValue, newValue) -> newHike.setSightWorthinesses(newValue));
        hikeSteps.textProperty().addListener((observable, oldValue, newValue) -> newHike.setSteps(newValue));
        hikeProTips.textProperty().addListener((observable, oldValue, newValue) -> newHike.setPracticalInformations(newValue));


        // Select A File :

        if (drawnTrace == null)
            configuringDirectoryChooser(directoryChooser);
        else {
            newHike.importDrawnTrace(drawnTrace);
        }
    }

    private void configuringDirectoryChooser(DirectoryChooser directoryChooser) {
        // Set title for DirectoryChooser
        directoryChooser.setTitle("Select a file");

        // Set Initial Directory
        directoryChooser.setInitialDirectory(new File(System.getProperty("user.home")));
    }

    public void selectDir() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Select a file");
        fileChooser.setInitialDirectory(new File(System.getProperty("user.dir")));
        fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("GPX File", "*.gpx"));
        File file = fileChooser.showOpenDialog(primaryStage);
        if (file != null) {
            String pathFile = file.getAbsolutePath();
            butSelectFile.setText(pathFile);
            resetHike();
            newHike.importFromGpx(pathFile);
        } else {
            butSelectFile.setText("Select a file");
        }
    }

    private void resetHike() {
        newHike = new Hike();
        newHike.addHikeListener(new HikeListener() {
            @Override
            public void hikeUpdated() {
                hikeName.setText(newHike.getName());
                hikeDescription.setText(newHike.getDescription());
                hikeCommune.setText(newHike.getCommune().getName());
                hikePostcode.setText(newHike.getCommune().getZipCode());

                hikeRegion.setText(newHike.getCommune().getRegion());
                hikePlaceToSee.setText(newHike.getSightWorthinesses());
                hikeProTips.setText(newHike.getPracticalInformations());
                hikeSteps.setText(newHike.getSteps());
                choiceDifficulty.getSelectionModel().select(newHike.getDifficulty());
                choiceActivityType.getSelectionModel().select(newHike.getActivityType());

                hikeDuration.setText(newHike.getDuration().toSeconds() != 0 ? "Computed duration : " + newHike.getDuration().toHoursPart() + "h" + String.format("%02d", newHike.getDuration().toMinutesPart()) : "");
            }

            @Override
            public void durationUpdated() {
                hikeDuration.setText(newHike.getDuration().toSeconds() != 0 ? "Computed duration : " + newHike.getDuration().toHoursPart() + "h" + String.format("%02d", newHike.getDuration().toMinutesPart()) : "");
            }
        });
    }

    public void cancel() {
        this.stageNewHike.close();
    }

    public void createNewHike() {
        this.hikes.addHike(newHike);
        this.stageNewHike.close();
    }

    public void checkCompute() {
        this.newHike.setDurationFromTrace(this.hikeComputeTime.isSelected());
    }

    public void activityChanged() {
        newHike.setActivityType(choiceActivityType.getValue());
    }

    public void difficultyChanged() {
        newHike.setDifficulty(choiceDifficulty.getValue());
    }
}
