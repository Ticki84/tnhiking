package TNHiking.controllers;

import TNHiking.enums.MarkerType;
import TNHiking.listeners.HikesListener;
import TNHiking.listeners.MapInitListener;
import TNHiking.models.CustomTraces;
import TNHiking.models.Hike;
import TNHiking.models.Hikes;
import com.sothawo.mapjfx.*;
import com.sothawo.mapjfx.event.MapViewEvent;
import io.jenetics.jpx.GPX;
import io.jenetics.jpx.Track;
import io.jenetics.jpx.TrackSegment;
import io.jenetics.jpx.WayPoint;
import javafx.fxml.FXML;
import javafx.scene.paint.Color;

import javax.swing.event.EventListenerList;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

public class ControllerMap {
    private static final int zoomThreshold = 12;
    private final EnumMap<MarkerType, ArrayList<Marker>> markers = new EnumMap<>(MarkerType.class);
    private final HashMap<Hike, CoordinateLine> lines = new HashMap<>();
    private final EventListenerList initListeners = new EventListenerList();
    private final Hikes hikes;
    private final CustomTraces customTraces;
    @FXML
    private com.sothawo.mapjfx.MapView mapView;
    private CoordinateLine buildingLine;

    public ControllerMap(Hikes hikes, CustomTraces customTraces) {
        MarkerType[] mt = MarkerType.class.getEnumConstants();
        for (MarkerType markerType : mt) {
            markers.put(markerType, new ArrayList<>());
        }
        this.hikes = hikes;
        this.customTraces = customTraces;
    }

    public void initialize() {
        mapView.setScaleShape(false);
        mapView.setPrefSize(Double.MAX_VALUE, Double.MAX_VALUE);

        mapView.zoomProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue.intValue() < zoomThreshold && oldValue.intValue() >= zoomThreshold) {
                onMapUnzoomed();
            } else if (newValue.intValue() > zoomThreshold && oldValue.intValue() <= zoomThreshold) {
                onMapZoomed();
            }
        });

        hikes.addHikesListener(new HikesListener() {
            @Override
            public void hikesUpdated() {
                onHikesUpdated();
            }

            @Override
            public void newHikeFavorite(Hike hike) {
            }
        });
        hikes.addMapPositionListener(this::updateMapExtent);
        customTraces.addTraceDrawnListener((trace) -> mapView.removeCoordinateLine(buildingLine));
        mapView.addEventHandler(MapViewEvent.MAP_CLICKED, this::onMapClicked);
        mapView.addEventHandler(MapViewEvent.MAP_BOUNDING_EXTENT, this::onMapExtentChanged);
    }

    public void onMapUnzoomed() {
        lines.forEach((hike, line) -> line.setVisible(false));
        markers.forEach((mType, markers) -> markers.forEach(marker -> updateMarkerUnzoomed(mType, marker)));
    }

    public void updateMarkerUnzoomed(MarkerType mType, Marker marker) {
        marker.setVisible(mType == MarkerType.HIKE);
    }

    public void onMapZoomed() {
        lines.forEach((hike, line) -> line.setVisible(true));
        markers.forEach((mType, markers) -> markers.forEach(marker -> updateMarkerZoomed(mType, marker)));
    }

    public void updateMarkerZoomed(MarkerType mType, Marker marker) {
        marker.setVisible(mType != MarkerType.HIKE);
    }

    public void updateMarkerVisibility(MarkerType mType, Marker marker) {
        if (mapView.getZoom() < zoomThreshold) {
            updateMarkerUnzoomed(mType, marker);
        } else {
            updateMarkerZoomed(mType, marker);
        }
    }

    public void updateLineVisibility(CoordinateLine line) {
        line.setVisible(!(mapView.getZoom() < zoomThreshold));
    }

    public void onMapInitialized() {
        fireInitListeners();
    }

    public void initMapAndControls() {
        mapView.setMapType(MapType.OSM);

        mapView.setCenter(new Coordinate(48.66882, 6.15542));
        mapView.initialize(Configuration.builder()
                .projection(Projection.WEB_MERCATOR)
                .showZoomControls(true)
                .build());

        mapView.initializedProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue) {
                onMapInitialized();
            }
        });
    }

    public void drawTrace(Hike hike) {
        GPX trace = hike.getTrace();
        for (Track track : trace.getTracks()) {
            for (TrackSegment segment : track.getSegments()) {
                List<Coordinate> points = segment.getPoints().stream()
                        .map(wayPoint -> new Coordinate(wayPoint.getLatitude().doubleValue(), wayPoint.getLongitude().doubleValue())
                        ).collect(Collectors.toList());
                drawLine(hike, points);
            }
        }
    }

    public void drawLine(Hike hike, List<Coordinate> points) {
        CoordinateLine line = new CoordinateLine(points);
        line.setVisible(true);
        line.setColor(Color.RED);
        updateLineVisibility(line);
        lines.put(hike, line);
        mapView.addCoordinateLine(line);
    }

    public void addMarker(Coordinate pos, MarkerType mt, String lbl) {
        String img;
        switch (mt) {
            case HIKE:
                img = "/images/greenmarker.png";
                break;
            case WAYPOINT:
                img = "/images/purplemarker.png";
                break;
            case PLACE:
                img = "/images/orangemarker.png";
                break;
            default:
                img = "/images/orangemarker.png";
        }
        Marker marker = new Marker(getClass().getResource(img), -22, -46)
                .setPosition(pos)
                .setVisible(true);
        if (lbl != null && !lbl.equals("")) {
            MapLabel WPLabel = new MapLabel(lbl, 10, -10).setVisible(true);
            marker.attachLabel(WPLabel);
        }
        updateMarkerVisibility(mt, marker);
        markers.get(mt).add(marker);
        mapView.addMarker(marker);
    }

    public void onHikesUpdated() {
        clearMap();
        hikes.getHikes().forEach(hike -> {
            drawTrace(hike);
            for (Track track : hike.getTrace().getTracks()) {
                for (TrackSegment segment : track.getSegments()) {
                    addMarker(toCoordinate(segment.getPoints().get(0)), MarkerType.HIKE, hike.getName());
                }
            }
            for (WayPoint wp : hike.getTrace().getWayPoints()) {
                addMarker(toCoordinate(wp), MarkerType.WAYPOINT, wp.getName().orElse(null));
            }
        });
    }

    public void clearMap() {
        lines.clear();
        markers.forEach((mType, markers) -> markers.clear());
    }

    public Coordinate toCoordinate(WayPoint wp) {
        return new Coordinate(wp.getLatitude().doubleValue(), wp.getLongitude().doubleValue());
    }

    public void addInitListener(MapInitListener listener) {
        initListeners.add(MapInitListener.class, listener);
    }

    public MapInitListener[] getInitListeners() {
        return initListeners.getListeners(MapInitListener.class);
    }

    protected void fireInitListeners() {
        for (MapInitListener listener : getInitListeners()) {
            listener.mapInitialized();
        }
    }

    public void updateMapExtent(Extent extent) {
        mapView.setExtent(extent);
        onMapZoomed();
    }

    public void onMapClicked(MapViewEvent event) {
        if (!customTraces.isBuildingTrace()) return;
        if (buildingLine != null) mapView.removeCoordinateLine(buildingLine);
        customTraces.addPoint(event.getCoordinate());
        buildingLine = new CoordinateLine(customTraces.getPoints()).setColor(Color.BLUE).setVisible(true);
        mapView.addCoordinateLine(buildingLine);
    }

    public void onMapExtentChanged(MapViewEvent event) {
        /*
        ArrayList<Hike> hikeList = hikes.getHikes();
        hikeList.forEach(hike -> {
            System.out.println(MapTools.shouldDraw(hike.getExtent(), event.getExtent()));
        });*/
        /*Coordinate center = mapView.getCenter();
        hikeList.sort((h1, h2) -> MapTools.compareHikes(center, h1, h2));
        lines.forEach((hike, line) ->  {
            if (MapTools.extentsIntersect(event.getExtent(), hike.getExtent())) {
                System.out.println(hike.getName());
            }
        });*/
    }
}
