package TNHiking.controllers;

import TNHiking.listeners.HikesListener;
import TNHiking.models.CustomTraces;
import TNHiking.models.Hike;
import TNHiking.models.Hikes;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.awt.*;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.HashMap;

public class ControllerPopup {
    private final Hikes hikes;
    private final CustomTraces customTraces;
    private final Stage primaryStage;
    private final HashMap<Hike, BorderPane> panes = new HashMap<>();
    public TextField textSearch;
    public Button butSearch;
    public Button butMore;
    public VBox vboxPreview;
    public Button butPlotter;

    public ControllerPopup(Stage primaryStage, Hikes hikes, CustomTraces customTraces) {
        this.hikes = hikes;
        this.customTraces = customTraces;
        this.textSearch = new TextField();
        this.butSearch = new Button();
        this.butMore = new Button();
        this.vboxPreview = new VBox();
        this.primaryStage = primaryStage;
    }

    public void initialize() {
        hikes.addHikesListener(new HikesListener() {
            @Override
            public void hikesUpdated() {
                onHikesUpdated();
            }

            @Override
            public void newHikeFavorite(Hike hike) {
                onNewFavorite(hike);
            }
        });
        customTraces.addTracesListener(this::onCustomTracesUpdate);
        this.butSearch.setFont(Font.font(12));
        this.butMore.setFont(Font.font(12));
        this.butPlotter.setFont(Font.font(12));
    }

    public void onHikesUpdated() {
        removeTraces();
        hikes.getHikes().forEach(hike -> {
            if (!hike.getFiltered()) {
                try {
                    addNewTrace(hike);
                } catch (IOException e) {
                    System.err.println("Error adding trace.");
                    e.printStackTrace();
                }
            }
        });
    }

    public void showPreviews() throws IOException {
        for (Hike hikeInstance : this.hikes.getHikes()) {
            addNewTrace(hikeInstance);
        }
    }

    public void onNewFavorite(Hike hike) {
        BorderPane pane = panes.get(hike);
        if (pane == null) return;
        vboxPreview.getChildren().remove(pane);
        vboxPreview.getChildren().add(0, pane);
    }

    public void biggerFontSearch() {
        this.butSearch.setFont(Font.font(14));
    }

    public void biggerFontMore() {
        this.butMore.setFont(Font.font(14));
    }

    public void biggerFontPlot() {
        this.butPlotter.setFont(Font.font(14));
    }

    public void tinierFontSearch() {
        this.butSearch.setFont(Font.font(12));
    }

    public void tinierFontMore() {
        this.butMore.setFont(Font.font(12));
    }

    public void tinierFontPlot() {
        this.butPlotter.setFont(Font.font(12));
    }


    public void addNewTrace(Hike hike) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/fxml/previewView.fxml"));
        ControllerPreview cp = new ControllerPreview(hikes, hike, this.primaryStage);
        loader.setControllerFactory(ic -> cp);
        BorderPane pane = loader.load();
        if (hike.getFavorite()) {
            panes.put(hike, pane);
            this.vboxPreview.getChildren().add(0, pane);
        } else {
            panes.put(hike, pane);
            this.vboxPreview.getChildren().add(pane);
        }
    }

    public void removeTraces() {
        this.vboxPreview.getChildren().clear();
    }

    public void search() {
        try {
            Desktop.getDesktop().browse(new URL("https://www.google.com").toURI());
        } catch (IOException | URISyntaxException e) {
            e.printStackTrace();
        }
    }

    public void onFilterChange() {
        String filterText = textSearch.getText();
        if (filterText == null) hikes.filterHikes();
        else hikes.filterHikes(filterText);
    }

    public void onPlotter() {
        if (customTraces.isBuildingTrace()) {
            customTraces.end();
        } else {
            customTraces.startTrace();
        }
    }

    public void onCustomTracesUpdate(boolean isActive) {
        String text = isActive ? "Plotting" : "Plot !";
        butPlotter.setText(text);
    }

    public void openSelect() {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/fxml/selectView.fxml"));
        Stage stageSelect = new Stage();
        ControllerSelect cs = new ControllerSelect(stageSelect, hikes);
        loader.setControllerFactory(ic -> cs);
        Parent root;
        try {
            root = loader.load();
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }
        Scene scene = new Scene(root, 500, 480);
        stageSelect.setScene(scene);
        stageSelect.setTitle("TNHiking : Select parameters");
        stageSelect.initModality(Modality.APPLICATION_MODAL);
        stageSelect.show();
    }
}
