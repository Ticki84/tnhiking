package TNHiking.controllers;

import TNHiking.enums.ActivityType;
import TNHiking.enums.Difficulty;
import TNHiking.models.Hike;
import io.jenetics.jpx.Length;
import io.jenetics.jpx.Track;
import io.jenetics.jpx.TrackSegment;
import io.jenetics.jpx.WayPoint;
import javafx.scene.chart.AreaChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;
import javafx.util.StringConverter;

import java.util.ArrayList;
import java.util.List;

public class ControllerSheet {

    private final Hike h;
    private final Stage s;
    public Label sh_title;
    public Label sh_time;
    public Label sh_type;
    public Label sh_distance;
    public Label sh_difficulty;
    public Label sh_elevation_gain;
    public Label sh_elevation_loss;
    public Label sh_highest_point;
    public Label sh_lowest_point;
    public Label sh_commune;
    public Label sh_region;
    public Label sh_description;
    public Label sh_information;
    public Label sh_to_see;
    public Label sh_steps;
    public ImageView sh_icon;
    public AreaChart<Number, Number> sh_elevationGraph;
    public NumberAxis sh_x;
    public NumberAxis sh_y;
    public ScrollPane scrollPictures;
    public VBox vbox;

    public ControllerSheet(Hike hike, Stage stage) {
        this.h = hike;
        this.s = stage;
    }

    public void initialize() {
        this.sh_title.setText(this.h.getName());
        this.sh_title.setStyle("-fx-font-weight: bold; -fx-font-size: 18; -fx-font-family: Bitstream-Vera-Sans-Mono");
        long minutes = this.h.getDuration().toMinutes();
        long hours = this.h.getDuration().toHours();
        minutes = minutes - 60 * hours;
        this.sh_time.setText(hours + "h " + minutes + "m");
        if (this.h.getActivityType() == ActivityType.FOOT) {
            this.sh_type.setText("Marche\nà pied");
            this.sh_icon.setImage(new Image(getClass().getResourceAsStream("/images/walking.png")));
        } else if (this.h.getActivityType() == ActivityType.BIKE) {
            this.sh_type.setText("Balade\nà vélo");
            this.sh_icon.setImage(new Image(getClass().getResourceAsStream("/images/biking.png")));
        } else if (this.h.getActivityType() == ActivityType.HORSE) {
            this.sh_type.setText("Randonnée\néquestre");
            this.sh_icon.setImage(new Image(getClass().getResourceAsStream("/images/riding.png")));
        } else if (this.h.getActivityType() == ActivityType.SKI) {
            this.sh_type.setText("Ski");
            this.sh_icon.setImage(new Image(getClass().getResourceAsStream("/images/skiing.png")));
        }
        int km = this.h.getDistance() / 1000;
        int m = (this.h.getDistance() - 1000 * km) / 100;
        this.sh_distance.setText(km + "," + m + " km");
        if (this.h.getDifficulty() == Difficulty.EASY) {
            this.sh_difficulty.setText("Facile");
        } else if (this.h.getDifficulty() == Difficulty.MEDIUM) {
            this.sh_difficulty.setText("Moyenne");
        } else if (this.h.getDifficulty() == Difficulty.HARD) {
            this.sh_difficulty.setText("Difficile");
        } else if (this.h.getDifficulty() == Difficulty.REALLY_HARD) {
            this.sh_difficulty.setText("Très difficile");
        }
        this.sh_elevation_gain.setText(this.h.getElevationGain() + " m");
        this.sh_elevation_loss.setText(this.h.getElevationGain() + " m");
        this.sh_highest_point.setText(this.h.getHighestSpot() + " m");
        this.sh_lowest_point.setText(this.h.getLowestSpot() + " m");
        this.sh_commune.setText(this.h.getCommune().getName() + " (" + this.h.getCommune().getZipCode() + ")");
        this.sh_region.setText(this.h.getCommune().getRegion());
        this.sh_description.setText(this.h.getDescription());
        this.sh_information.setText(this.h.getPracticalInformations());
        this.sh_to_see.setText(this.h.getSightWorthinesses());
        this.sh_steps.setText(this.h.getSteps());

        List<Track> tracks = h.getTrace().getTracks();
        int miny = 10000;
        int maxy = 0;
        int maxx = 0;
        for (int i = 0; i < tracks.size(); i++) {
            XYChart.Series<Number, Number> series = new XYChart.Series<>();
            series.setName("Track " + (i + 1));
            if (tracks.size() < 2) sh_elevationGraph.setLegendVisible(false);
            int trackDist = 0;
            for (TrackSegment segment : tracks.get(i).getSegments()) {
                List<WayPoint> wp = segment.getPoints();
                WayPoint orig = wp.get(0);
                for (int j = 0; j < wp.size(); j += (wp.size() / 100 > 0 ? wp.size() / 100 : 1)) {
                    int y = wp.get(j).getElevation().orElse(Length.of(0, Length.Unit.METER)).intValue();
                    miny = Math.min(y, miny);
                    maxy = Math.max(y, maxy);
                    int x = trackDist + Hike.distanceBeetween(segment, orig, wp.get(j));
                    XYChart.Data<Number, Number> data = new XYChart.Data<>(x, y);
                    Rectangle rect = new Rectangle(0, 0);
                    rect.setVisible(false);
                    data.setNode(rect);
                    series.getData().add(data);
                }
                trackDist += Hike.distanceBeetween(segment, orig, wp.get(wp.size() - 1));
                maxx = Math.max(maxx, trackDist);
            }
            sh_elevationGraph.getData().add(series);
        }
        int gap = (int) (Math.abs(maxy - miny) * 0.2f);
        sh_y.setLowerBound(miny - 2 * gap);
        if (sh_y.getLowerBound() < 0) sh_y.setLowerBound(0);
        sh_y.setUpperBound(maxy + gap);
        sh_y.setAutoRanging(false);
        sh_y.setLabel("Altitude");
        sh_x.setLabel("Distance");
        if (maxx >= 1000) {
            sh_x.setTickLabelFormatter(new StringConverter<>() {
                @Override
                public String toString(Number object) {
                    return String.format("%.2fkm", object.intValue() / 1000f);
                }

                @Override
                public Number fromString(String string) {
                    return 0;
                }
            });
        } else {
            sh_x.setTickLabelFormatter(new StringConverter<>() {
                @Override
                public String toString(Number object) {
                    return String.format("%dm", object.intValue());
                }

                @Override
                public Number fromString(String string) {
                    return 0;
                }
            });
        }
        sh_y.setTickLabelFormatter(new StringConverter<>() {
            @Override
            public String toString(Number object) {
                return String.format("%dm", object.intValue());
            }

            @Override
            public Number fromString(String string) {
                return 0;
            }
        });


        // add pictures :
        this.vbox = new VBox();
        ArrayList<String> listLinkImg = this.h.getPhotos();
        if (listLinkImg != null) {
            for (String nameImg : listLinkImg) {
                Image img = new Image(nameImg);
                ImageView imgView = new ImageView(img);
                imgView.setFitHeight(380);
                imgView.setFitWidth(380);
                imgView.setPreserveRatio(true);
                this.vbox.getChildren().add(imgView);
            }
        }
        this.scrollPictures.setContent(this.vbox);
    }

    public void returnToMap() {
        this.s.close();
    }
}
