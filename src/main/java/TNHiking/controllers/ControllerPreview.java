package TNHiking.controllers;

import TNHiking.enums.ActivityType;
import TNHiking.enums.Difficulty;
import TNHiking.models.Hike;
import TNHiking.models.Hikes;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class ControllerPreview {

    private final Hike h;
    private final Hikes hikes;
    private final Stage stage;
    public ImageView icon;
    public Label time;
    public Label type;
    public Label distance;
    public Label difficulty;
    public Label elevationGain;
    public Label elevationLoss;
    public Label title;
    public BorderPane bpPreview;
    public ImageView thumbnail;

    public ControllerPreview(Hikes hikes, Hike hike, Stage stage) {
        this.hikes = hikes;
        this.h = hike;
        this.stage = stage;
    }


    public void initialize() {
        this.title.setText(this.h.getName());
        this.title.setStyle("-fx-font-weight: bold; -fx-font-size: 18; -fx-font-family: Bitstream-Vera-Sans-Mono");
        if (this.h.getActivityType() == ActivityType.FOOT) {
            this.type.setText("Marche\nà pied");
            this.icon.setImage(new Image(getClass().getResourceAsStream("/images/walking.png")));
        } else if (this.h.getActivityType() == ActivityType.BIKE) {
            this.type.setText("Balade\nà vélo");
            this.icon.setImage(new Image(getClass().getResourceAsStream("/images/biking.png")));
        } else if (this.h.getActivityType() == ActivityType.HORSE) {
            this.type.setText("Randonnée\néquestre");
            this.icon.setImage(new Image(getClass().getResourceAsStream("/images/riding.png")));
        } else if (this.h.getActivityType() == ActivityType.SKI) {
            this.type.setText("Ski");
            this.icon.setImage(new Image(getClass().getResourceAsStream("/images/skiing.png")));
        }
        if (this.h.getDifficulty() == Difficulty.EASY) {
            this.difficulty.setText("Facile");
        } else if (this.h.getDifficulty() == Difficulty.MEDIUM) {
            this.difficulty.setText("Moyenne");
        } else if (this.h.getDifficulty() == Difficulty.HARD) {
            this.difficulty.setText("Difficile");
        } else if (this.h.getDifficulty() == Difficulty.REALLY_HARD) {
            this.difficulty.setText("Très\ndifficile");
        }
        long minutes = this.h.getDuration().toMinutes();
        long hours = this.h.getDuration().toHours();
        minutes = minutes - 60 * hours;
        this.time.setText(hours + "h " + minutes + "m");
        int km = this.h.getDistance() / 1000;
        int m = (this.h.getDistance() - 1000 * km) / 100;
        this.distance.setText(km + "," + m + " km");
        this.elevationGain.setText(this.h.getElevationGain() + " m");
        this.elevationLoss.setText(this.h.getElevationLoss() + " m");

        this.bpPreview.setStyle("-fx-background-insets: 0,1,4,5 ;-fx-background-radius: 9,8,5,4 ;-fx-effect:  dropshadow( three-pass-box , rgba(255,255,255,0.2) , 1, 0.0 , 0 , 1) ;-fx-background-color: linear-gradient(#686868 0%, #232723 25%, #373837 75%, #757575 100%),   linear-gradient(#b9b9b9 0%, #c2c2c2 20%, #afafaf 80%, #c8c8c8 100%),        linear-gradient(#f5f5f5 0%, #dbdbdb 50%, #cacaca 51%, #d7d7d7 100%)");

        ArrayList<String> listLinkImage = this.h.getPhotos();
        if (listLinkImage.size() > 0) {
            String nameImage = listLinkImage.get(0);
            Image image = new Image(nameImage);
            this.thumbnail.setImage(image);
        }

    }

    public void enterBp() {
        if (this.h.getFavorite()) {
            this.bpPreview.setStyle("-fx-background-insets: 0,1,4,5 ;-fx-background-radius: 9,8,5,4 ; -fx-effect:  dropshadow( three-pass-box , rgba(255,255,255,0.2) , 1, 0.0 , 0 , 1)  ;-fx-background-color:linear-gradient(from 0% 93% to 0% 100%, #a34313 0%, #903b12 100%),\n" + "        #e0dc23,\n" + "        #f3ef28,\n" + "        radial-gradient(center 50% 50%, radius 100%, #fffb00, #d7d7d7)");

        } else {
            this.bpPreview.setStyle("-fx-background-insets: 0,1,4,5 ;-fx-background-radius: 9,8,5,4 ; -fx-effect:  dropshadow( three-pass-box , rgba(255,255,255,0.2) , 1, 0.0 , 0 , 1)  ;-fx-background-color:linear-gradient(from 0% 93% to 0% 100%, #a34313 0%, #903b12 100%),\n" + "        #8f9cea,\n" + "        #a2adf4,\n" + "        radial-gradient(center 50% 50%, radius 100%, #b4beff, #d7d7d7)");
        }

    }

    public void leaveBp() {
        this.bpPreview.setStyle("-fx-background-insets: 0,1,4,5 ;-fx-background-radius: 9,8,5,4 ;-fx-effect:  dropshadow( three-pass-box , rgba(255,255,255,0.2) , 1, 0.0 , 0 , 1) ;-fx-background-color: linear-gradient(#686868 0%, #232723 25%, #373837 75%, #757575 100%),   linear-gradient(#b9b9b9 0%, #c2c2c2 20%, #afafaf 80%, #c8c8c8 100%),        linear-gradient(#f5f5f5 0%, #dbdbdb 50%, #cacaca 51%, #d7d7d7 100%)");
    }


    public void onActionBp(MouseEvent mouseEvent) throws IOException {

        if (mouseEvent.getButton().equals(MouseButton.PRIMARY)) {
            switch (mouseEvent.getClickCount()) {
                case 1:
                    hikes.updateMapExtent(h.getExtent());
                    break;
                case 2:
                    FXMLLoader loader = new FXMLLoader();
                    loader.setLocation(getClass().getResource("/fxml/sheetView.fxml"));
                    Stage stageSheet = new Stage();
                    ControllerSheet cs = new ControllerSheet(this.h, stageSheet);
                    loader.setControllerFactory(ic -> cs);
                    Parent root = loader.load();
                    Scene scene = new Scene(root, 1400, 890);
                    stageSheet.setScene(scene);
                    stageSheet.setTitle("TNHiking : " + h.getName());
                    stageSheet.initModality(Modality.APPLICATION_MODAL);

                    stageSheet.show();
                    break;
            }
        }
    }


    public void onRightClicBp() {
        Hikes hikes = this.hikes;
        ContextMenu ctxtMenu = new ContextMenu();
        MenuItem item1 = new MenuItem("Toggle Fav'");
        item1.setOnAction(actionEvent -> hikes.setFavorite(h, !h.getFavorite()));

        MenuItem item2 = new MenuItem("Show more details");

        item2.setOnAction(new EventHandler<>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                FXMLLoader loader = new FXMLLoader();
                loader.setLocation(getClass().getResource("/fxml/sheetView.fxml"));
                Stage stageSheet = new Stage();
                ControllerSheet cs = new ControllerSheet(h, stageSheet);
                loader.setControllerFactory(ic -> cs);
                try {
                    Parent root = loader.load();

                    Scene scene = new Scene(root, 1400, 800);
                    stageSheet.setScene(scene);
                    stageSheet.setTitle("TNHiking : " + h.getName());
                    stageSheet.initModality(Modality.APPLICATION_MODAL);

                    stageSheet.show();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        MenuItem item3 = new MenuItem("See on the map");

        item3.setOnAction(actionEvent -> hikes.updateMapExtent(h.getExtent()));

        MenuItem item4 = new MenuItem("Export to GPX");

        item4.setOnAction(actionEvent -> {
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Save hike as...");
            fileChooser.setInitialFileName(ControllerPreview.this.h.getName().replaceAll("[ &\\/\\\\#,+()$~%.'\":*?<>{}]", "_").replaceAll("[\\u0300-\\u036f]", "") + ".gpx");
            fileChooser.setInitialDirectory(new File(System.getProperty("user.dir")));
            fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("GPX file", "*.gpx"));
            File file = fileChooser.showSaveDialog(stage);
            if (file != null) {
                h.exportToGpx(file.getAbsolutePath());
            }
        });


        MenuItem item5 = new MenuItem("Delete");

        item5.setOnAction(actionEvent -> hikes.removeHike(h));


        ctxtMenu.getItems().addAll(item1, item2, item3, item4, item5);
        this.bpPreview.setOnContextMenuRequested(contextMenuEvent -> ctxtMenu.show(bpPreview, contextMenuEvent.getScreenX(), contextMenuEvent.getScreenY()));
    }
}
