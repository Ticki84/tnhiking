package TNHiking.controllers;

import TNHiking.enums.ActivityType;
import TNHiking.enums.Difficulty;
import TNHiking.models.Hikes;
import javafx.collections.FXCollections;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.stage.Stage;

import java.util.ArrayList;

public class ControllerSelect {

    private final Stage here;
    private final Hikes hikes;
    public ComboBox<String> select_activity;
    public ComboBox<String> select_difficulty;
    public Slider select_slider_duration_min;
    public Label select_duration_min;
    public Slider select_slider_duration_max;
    public Label select_duration_max;
    public Slider select_slider_distance_min;
    public Label select_distance_min;
    public Slider select_slider_distance_max;
    public Label select_distance_max;

    public ControllerSelect(Stage stage, Hikes hikes) {
        this.here = stage;
        this.hikes = hikes;
    }

    public void initialize() {

        // Select Activity Type :
        ArrayList<String> activities = new ArrayList<>();
        activities.add("Toutes");
        activities.add("Marche à pied");
        activities.add("Balade à vélo");
        activities.add("Randonnée équestre");
        activities.add("Ski");
        select_activity.setItems(FXCollections.observableArrayList(activities));
        select_activity.getSelectionModel().selectFirst();

        // Select Difficulty :
        ArrayList<String> difficulties = new ArrayList<>();
        difficulties.add("Toutes");
        difficulties.add("Facile");
        difficulties.add("Moyenne");
        difficulties.add("Difficile");
        difficulties.add("Très Difficile");
        select_difficulty.setItems(FXCollections.observableArrayList(difficulties));
        select_difficulty.getSelectionModel().selectFirst();

        // Initialize sliders
        select_slider_duration_min.setMin(0);
        select_slider_duration_min.setMax(100);
        select_slider_duration_min.valueProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue.intValue() < 24) {
                select_duration_min.setText(newValue.intValue() + " heures");
            } else {
                select_duration_min.setText(newValue.intValue() / 24 + " jours et " + (newValue.intValue() - (newValue.intValue() / 24) * 24) + " heures");
            }
        });
        //
        select_slider_duration_max.setMin(0);
        select_slider_duration_max.setMax(100);
        select_slider_duration_max.setValue(100);
        select_slider_duration_max.valueProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue.intValue() < 24) {
                select_duration_max.setText(newValue.intValue() + " heures");
            } else {
                select_duration_max.setText(newValue.intValue() / 24 + " jours et " + (newValue.intValue() - (newValue.intValue() / 24) * 24) + " heures");
            }
        });
        //
        select_slider_distance_min.setMin(0);
        select_slider_distance_min.setMax(500);
        select_slider_distance_min.valueProperty().addListener((observable, oldValue, newValue) -> select_distance_min.setText(newValue.intValue() + " km"));
        //
        select_slider_distance_max.setMin(0);
        select_slider_distance_max.setMax(500);
        select_slider_distance_max.setValue(500);
        select_slider_distance_max.valueProperty().addListener((observable, oldValue, newValue) -> select_distance_max.setText(newValue.intValue() + " km"));
    }

    public void validate() {
        ActivityType activity = getActivityTypes();
        Difficulty difficulty = getDifficulties();
        Double minDuration = select_slider_duration_min.getValue();
        Double maxDuration = select_slider_duration_max.getValue();
        Double minDistance = select_slider_distance_min.getValue();
        Double maxDistance = select_slider_distance_max.getValue();

        hikes.filterHikes(activity, difficulty, minDuration, maxDuration, minDistance, maxDistance);
    }

    private ActivityType getActivityTypes() {
        switch (select_activity.getSelectionModel().getSelectedItem()) {
            case "Toutes":
                return ActivityType.ALL;
            case "Marche à pied":
                return ActivityType.FOOT;
            case "Balade à vélo":
                return ActivityType.BIKE;
            case "Randonnée équestre":
                return ActivityType.HORSE;
            case "Ski":
                return ActivityType.SKI;
            default:
                return null;
        }
    }

    private Difficulty getDifficulties() {
        switch (select_difficulty.getSelectionModel().getSelectedItem()) {
            case "Toutes":
                return Difficulty.ALL;
            case "Facile":
                return Difficulty.EASY;
            case "Moyenne":
                return Difficulty.MEDIUM;
            case "Difficile":
                return Difficulty.HARD;
            case "Très Difficile":
                return Difficulty.REALLY_HARD;
            default:
                return null;
        }
    }

    public void cancel() {
        this.here.close();
    }
}
