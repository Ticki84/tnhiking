package TNHiking.controllers;

import TNHiking.models.CustomTraces;
import TNHiking.models.Hike;
import TNHiking.models.Hikes;
import io.jenetics.jpx.GPX;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.awt.*;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;

public class ControllerMain {


    private final Stage primaryStage;
    private final Hikes hikes;
    public MenuItem butClose;
    public VBox popupView;
    public MenuItem butNewTrace;
    public MenuItem butDelete;

    public ControllerMain(Stage primaryStage, Hikes hikes, CustomTraces customTraces) {
        this.butClose = new MenuItem();
        this.popupView = new VBox();
        this.primaryStage = primaryStage;
        this.hikes = hikes;

        customTraces.addTraceDrawnListener(this::newTrace);
    }

    public void close() {
        Platform.exit();
    }

    public void newTrace(GPX trace) {
        FXMLLoader loader = new FXMLLoader();
        Stage stageNewTrace = new Stage();
        loader.setLocation(getClass().getResource("/fxml/newHikeView.fxml"));
        ControllerNewHike cnh;
        if (trace != null)
            cnh = new ControllerNewHike(primaryStage, hikes, stageNewTrace, trace);
        else
            cnh = new ControllerNewHike(primaryStage, hikes, stageNewTrace);
        loader.setControllerFactory(ic -> cnh);
        Parent root;
        try {
            root = loader.load();
        } catch (IOException e) {
            System.err.println("Error loading newHikeView.");
            e.printStackTrace();
            return;
        }
        Scene scene = new Scene(root, 660, 780);
        stageNewTrace.setScene(scene);
        stageNewTrace.setTitle("TNHiking : Add a new hike");
        stageNewTrace.initModality(Modality.APPLICATION_MODAL);

        stageNewTrace.show();
    }

    public void onNewTrace() {
        newTrace(null);
    }

    public void deleteTrace() {
        for (Hike h : hikes.getHikes()) {
            hikes.removeHike(h);
        }
    }

    public void surprise() {
        try {
            Desktop.getDesktop().browse(new URL("https://www.youtube.com/watch?v=dQw4w9WgXcQ").toURI());
        } catch (IOException | URISyntaxException e) {
            e.printStackTrace();
        }
    }

}
