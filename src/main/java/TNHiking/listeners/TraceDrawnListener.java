package TNHiking.listeners;

import io.jenetics.jpx.GPX;

import java.util.EventListener;

public interface TraceDrawnListener extends EventListener {
    void traceFinished(GPX trace);
}
