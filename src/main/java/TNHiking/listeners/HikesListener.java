package TNHiking.listeners;

import TNHiking.models.Hike;

import java.util.EventListener;

public interface HikesListener extends EventListener {
    void hikesUpdated();

    void newHikeFavorite(Hike hike);
}
