package TNHiking.listeners;

import java.util.EventListener;

public interface CustomTracesListener extends EventListener {
    void traceUpdated(boolean isActive);
}
