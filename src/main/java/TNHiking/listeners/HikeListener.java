package TNHiking.listeners;

import java.util.EventListener;

public interface HikeListener extends EventListener {
    void hikeUpdated();

    void durationUpdated();
}
