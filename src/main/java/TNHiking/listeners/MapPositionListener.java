package TNHiking.listeners;

import com.sothawo.mapjfx.Extent;

import java.util.EventListener;

public interface MapPositionListener extends EventListener {
    void mapCenterUpdated(Extent extent);
}
