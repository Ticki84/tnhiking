package TNHiking.listeners;

import java.util.EventListener;

public interface MapInitListener extends EventListener {
    void mapInitialized();
}
