module TNHiking {
    requires java.desktop;

    requires javafx.base;
    requires javafx.graphics;
    requires javafx.controls;
    requires javafx.fxml;
    requires java.xml;

    requires com.sothawo.mapjfx;

    requires io.jenetics.jpx;

    requires java.sql;

    requires org.json;
    requires sqlite.jdbc;

    opens TNHiking.controllers;

    exports TNHiking;
    exports TNHiking.controllers;
    exports TNHiking.enums;
    exports TNHiking.models;
    exports TNHiking.listeners;
}
